package update.mercurial;

import static java.util.concurrent.TimeUnit.MINUTES;
import static models.PullReviewEventType.PULL_REVIEW_GENERATED;
import static models.PullReviewEventType.WEBREV_GENERATED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import models.AbstractPersistenceIntegrationTest;
import models.PullReview;
import models.PullReviewEvent;
import models.User;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.junit.ClassRule;
import org.junit.Test;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import update.ActorRule;
import akka.actor.ActorRef;
import akka.event.EventStream;

public class WebrevGeneratorST extends AbstractPersistenceIntegrationTest {

    @ClassRule
    public static ActorRule actorRule = new ActorRule();

    private static final String DONT_CARE = "";

    @Test
    public void generatesWebrev() throws Exception {
        User adoptopenjdk = User.findOrCreate("adoptopenjdk", "adoptopenjdk");
        PullReview pullReview = new PullReview("jdk8-jdk", "1", DONT_CARE, DONT_CARE, adoptopenjdk, DateTime.now(), DateTime.now(), "default");
        File webrevLocation = pullReview.webrevLocation();

        PullReviewEvent message = new PullReviewEvent(PULL_REVIEW_GENERATED);
        message.pullReview = pullReview;

        try {
            EventStream eventStream = actorRule.eventStream();
            ActorRef actor = actorRule.actorOf(WebrevGenerator.class);
            eventStream.subscribe(actor, PullReviewEvent.class);
            eventStream.publish(message);

            Future<PullReviewEvent> response = actorRule.expectMsg(PullReviewEvent.class, MINUTES.toMillis(5));

            PullReviewEvent reply = Await.result(response, Duration.Inf());
            assertEquals(WEBREV_GENERATED, reply.pullReviewEventType);

            assertTrue(webrevLocation.exists());
            assertTrue(webrevLocation.isDirectory());
            assertTrue(webrevLocation.list().length > 1);
        } finally {
            FileUtils.deleteDirectory(webrevLocation);
        }
    }
    
}
