package update;

import com.icegreen.greenmail.util.GreenMail;
import org.junit.rules.ExternalResource;

import javax.mail.internet.MimeMessage;
import java.util.Arrays;
import java.util.List;

/**
 * Starts and stops a fake mail server.
 */
public class GreenMailRule extends ExternalResource {

    private final GreenMail mailServer;

    public GreenMailRule() {
        mailServer = new GreenMail();
    }

    public List<MimeMessage> getReceivedMessages() {
        return Arrays.asList(mailServer.getReceivedMessages());
    }

    @Override
    protected void before() throws Throwable {
        mailServer.start();
    }

    @Override
    protected void after() {
        mailServer.stop();
    }
}
