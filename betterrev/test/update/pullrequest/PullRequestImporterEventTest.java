package update.pullrequest;

import akka.actor.ActorRef;
import models.*;
import org.joda.time.DateTime;
import org.junit.Test;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.Collection;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * PullRequestImporterEventTest
 */
public class PullRequestImporterEventTest extends AbstractPullRequestImporterTest {

    private static final String TEST_REPOSITORY_ID = "123";
    private static final String TEST_PULL_REQUEST_ID = "123";
    private static final String TEST_PULL_REVIEW_NAME = "Major Mods!";
    private static final String TEST_PULL_REVIEW_DESCRIPTION = "Some change to the code";
    private static final DateTime TEST_PULL_REVIEW_CREATED_ON = DateTime.now();
    private static final DateTime TEST_PULL_REVIEW_UPDATED_ON = DateTime.now();
    private static final String TEST_BRANCH_NAME = "sigh";

    @Test
    public void importAllReviews_publishesPullReviewGeneratedEvents() throws Exception {
        // setup PullRequestImporter actor
        ActorRef pullRequestImporterActor = actorRule.actorOf(PullRequestImporter.class);
        eventStream.subscribe(pullRequestImporterActor, ImportPullRequestsEvent.class);

        // setup receiver
        Future<PullReviewEvent> response = actorRule.expectMsg(PullReviewEvent.class, SECONDS.toMillis(10));

        // trigger pull request import from json
        eventStream.publish(new ImportPullRequestsEvent(firstResponse, repositoryId));

        // verify events sent
        PullReviewEvent reply = Await.result(response, Duration.Inf());
        assertEquals(PullReviewEventType.PULL_REVIEW_GENERATED, reply.pullReviewEventType);
        assertEquals(State.OPEN, reply.pullReview.state);
    }

    @Test
    public void shouldReturn_One_PullReviewEventFilteredByType_PULL_REVIEW_GENERATED() {
        PullReview pullReview = createTestInstance();
        pullReview.save();

        Collection<PullReviewEvent> pullReviewEvents = pullReview.pullReviewEvents;
        assertThat(pullReviewEvents.size(), is(1));

        PullReviewEvent firstPullReviewEvent = pullReviewEvents.iterator().next();
        assertThat(firstPullReviewEvent.pullReviewEventType,
                is(PullReviewEventType.PULL_REVIEW_GENERATED));
    }

    public static PullReview createTestInstance() {
        PullReview pullReview = new PullReview(TEST_REPOSITORY_ID, TEST_PULL_REQUEST_ID,
                TEST_PULL_REVIEW_NAME, TEST_PULL_REVIEW_DESCRIPTION, UserTest.createTestInstance(),
                TEST_PULL_REVIEW_CREATED_ON, TEST_PULL_REVIEW_UPDATED_ON, TEST_BRANCH_NAME);

        pullReview.tags.add(TagTest.createTestInstance());
        pullReview.pullReviewEvents.add(new PullReviewEvent(PullReviewEventType.PULL_REVIEW_GENERATED));

        return pullReview;
    }
}
