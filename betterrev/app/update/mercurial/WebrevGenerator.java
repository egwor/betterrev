package update.mercurial;

import models.PullReview;
import models.PullReviewEvent;
import models.PullReviewEventType;
import play.Logger;
import update.BetterrevActor;
import utils.Processes;

import java.io.IOException;

import static models.PullReviewEventType.*;

/**
 * Generates webrevs based upon a pullreview diffed against the current openjdk repository
 */
public class WebrevGenerator extends BetterrevActor {

    private static final String WEBREV_SCRIPT = "adopt/generate_webrev.sh";

    private String adoptDirectory;

    public WebrevGenerator() {
        this.adoptDirectory = "adopt";
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (!(message instanceof PullReviewEvent)) {
            unhandled(message);
            return;
        }

        PullReviewEvent event = (PullReviewEvent) message;
        PullReviewEventType type = event.pullReviewEventType;
        if (type != PULL_REVIEW_GENERATED && type != PULL_REVIEW_MODIFIED) {
            return;
        }

        Logger.debug("PullReviewEvent of type " + type + " received.");

        int exitCode = generateWebrev(event);

        if (exitCode != 0) {
//            TODO: proper error handling around this process
//            Also bear in mind exceptions
        }

        PullReviewEvent webRevGenerated = new PullReviewEvent(WEBREV_GENERATED);
        eventStream().publish(webRevGenerated);
    }

    private int generateWebrev(PullReviewEvent event) throws IOException {
        PullReview pullReview = event.pullReview;
        String repository = pullReview.openJdkRepoName();
        String remote = pullReview.requestersRepositoryUrl();
        String resultLocation = pullReview.webrevLocation().getPath();

        int exitCode = Processes.runThroughShell(adoptDirectory, WEBREV_SCRIPT, repository, remote, resultLocation);
        return exitCode;
    }

}
