package update.pullrequest;

import models.PullReview;

import java.util.List;

/**
 * @author Edward Yue Shung Wong
 */
public final class PullRequestsImportedEvent {
    private List<PullReview> pullReviews;

    public PullRequestsImportedEvent(List<PullReview> pullReviews) {
        this.pullReviews = pullReviews;
    }

    public List<PullReview> getPullReviews() {
        return pullReviews;
    }
}
