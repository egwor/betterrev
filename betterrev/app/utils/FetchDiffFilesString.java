package utils;

import play.libs.WS;

public final class FetchDiffFilesString {
    private FetchDiffFilesString() {
        //  Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
    }

    public static String from(String inPullRequestURL) {
        WS.Response response = WS.url(inPullRequestURL).get().get();
        return response.getBody();
    }
}
