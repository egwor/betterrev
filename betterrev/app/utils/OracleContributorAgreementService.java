package utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

public class OracleContributorAgreementService {

	private final static String OCA_URL = "http://www.oracle.com/technetwork/community/oca-486395.html";
	private final static String OPEN_JDK = "OpenJDK";

	public static boolean hasSignedOCAforOpenJDK(String userFullName)
			throws MalformedURLException, IOException {

		TagNode table = loadTableOCAsignedUsers();

		@SuppressWarnings("unchecked")
		List<TagNode> LiElements = table.getElementListByName("li", true);
		for (TagNode liElement : LiElements) {
			String contributorDetails = liElement.getText().toString();
			if (contributorDetails.contains(userFullName)
					&& contributorDetails.contains(OPEN_JDK)) {
				return true;
			}
		}
		return false;
	}

	private static TagNode loadTableOCAsignedUsers() throws IOException,
			MalformedURLException {
		CleanerProperties props = initialiseCleanerProperties();
		boolean recursiveSearchTag = true;
		boolean caseSensitiveTag = false;
		TagNode tagNode = new HtmlCleaner(props).clean(new URL(OCA_URL));
		TagNode[] tables = tagNode.getElementsByAttValue("class", "dataTable",
				recursiveSearchTag, caseSensitiveTag);
		return tables[0];
	}

	private static CleanerProperties initialiseCleanerProperties() {
		CleanerProperties props = new CleanerProperties();
		props.setTranslateSpecialEntities(true);
		props.setTransResCharsToNCR(true);
		props.setOmitComments(true);
		return props;
	}

}
